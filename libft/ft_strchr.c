/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 20:16:56 by apietush          #+#    #+#             */
/*   Updated: 2016/11/27 21:01:39 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	if (!*s)
		return (0);
	if (c == 0)
		return ((char*)&s[ft_strlen(s)]);
	while (*s && *s != c)
		s++;
	if (*s == c)
		return ((char*)s);
	else
		return (0);
}
