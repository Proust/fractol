/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 14:54:27 by apietush          #+#    #+#             */
/*   Updated: 2016/12/07 16:33:22 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*new;
	size_t	i;
	size_t	j;

	j = -1;
	i = -1;
	if (!s1 || !s2)
		return (NULL);
	if ((new = (char *)malloc(sizeof(char*) *
		(1 + ft_strlen(s1) + ft_strlen(s2)))) == NULL)
		return (NULL);
	while (s1[++i])
		new[i] = s1[i];
	while (s2[++j])
	{
		new[i] = s2[j];
		i++;
	}
	new[i] = '\0';
	return (new);
}
