/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 21:10:07 by apietush          #+#    #+#             */
/*   Updated: 2016/12/02 23:14:46 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char *s;
	unsigned char *d;

	d = (unsigned char*)dst;
	s = (unsigned char*)src;
	while (n--)
	{
		if (*s == (unsigned char)c)
		{
			*d = *s;
			return (++d);
		}
		*d = *s;
		d++;
		s++;
	}
	return (NULL);
}
