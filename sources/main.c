/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 21:31:01 by apietush          #+#    #+#             */
/*   Updated: 2017/10/07 21:31:02 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	render_fract(t_frac *frac)
{
	int bits_per_pixel;
	int size_line;
	int endian;

	frac->mlx.img = mlx_new_image(frac->mlx.mlx_ptr, WIN_WIDTH, WIN_HEIGHT);
	frac->mlx.arr = mlx_get_data_addr(frac->mlx.img,
				&bits_per_pixel, &size_line, &endian);
	if (frac->type == 1)
		draw_julia(frac);
	else if (frac->type == 2)
		draw_mb(frac);
	else if (frac->type == 3)
		draw_ship(frac);
	else if (frac->type == 4)
		draw_dust(frac);
	else
		draw_star(frac);
	mlx_put_image_to_window(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
											frac->mlx.img, 0, 0);
	put_text(frac);
	mlx_destroy_image(frac->mlx.mlx_ptr, frac->mlx.img);
}

void	mlx_in(t_frac *frac)
{
	frac->mlx.mlx_ptr = mlx_init();
	frac->mlx.win_ptr = mlx_new_window(frac->mlx.mlx_ptr,
						WIN_WIDTH, WIN_HEIGHT, "fractol");
	if (frac->type == 1 || frac->type == 5 || frac->type == 4)
		set_start_julia(frac);
	else if (frac->type == 2 || frac->type == 3)
		set_start_mb_bs(frac);
	render_fract(frac);
	mlx_hook(frac->mlx.win_ptr, 6, 1L << 6, motion, frac);
	mlx_mouse_hook(frac->mlx.win_ptr, zoom_event, frac);
	mlx_hook(frac->mlx.win_ptr, 17, 1L << 17, exit_x, frac);
	mlx_hook(frac->mlx.win_ptr, 2, 5, key_hook, frac);
	mlx_loop(frac->mlx.mlx_ptr);
}

void	usage_error(void)
{
	ft_putstr("Usage: ./fractol\n 1 - Julia\n 2 - Maldenbrot\n");
	ft_putstr(" 3 - Burning ships\n 4 - Cosmic dust \n 5 - Stars \n");
	exit(0);
}

void	memory_error(void)
{
	ft_putstr("memory error");
	exit(0);
}

int		main(int argc, char **argv)
{
	t_frac	*frac;

	if (!(frac = (t_frac*)malloc(sizeof(t_frac))))
		memory_error();
	if (argc != 2)
		usage_error();
	if (ft_strcmp(argv[1], "1") != 0 && ft_strcmp(argv[1], "2")
		!= 0 && ft_strcmp(argv[1], "4") != 0 && ft_strcmp(argv[1], "3") != 0
		&& ft_strcmp(argv[1], "5") != 0)
		usage_error();
	if (ft_strcmp(argv[1], "1") == 0)
		frac->type = 1;
	if (ft_strcmp(argv[1], "2") == 0)
		frac->type = 2;
	if (ft_strcmp(argv[1], "3") == 0)
		frac->type = 3;
	if (ft_strcmp(argv[1], "4") == 0)
		frac->type = 4;
	if (ft_strcmp(argv[1], "5") == 0)
		frac->type = 5;
	mlx_in(frac);
	return (0);
}
