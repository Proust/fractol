/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cosmic_dust.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 14:28:22 by apietush          #+#    #+#             */
/*   Updated: 2017/10/12 14:28:23 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	set_start_dust(t_frac *frac)
{
	frac->zoom = 1;
	frac->movex = 0;
	frac->movey = 0;
	frac->maxiter = 50;
	frac->cre = -0.8;
	frac->cim = 0.156;
	frac->stop = 0;
}

void	dust_inside(t_frac *frac)
{
	frac->oldre = sin(frac->newre);
	frac->oldim = cos(frac->newim);
	frac->newre = frac->oldre * frac->oldre - frac->oldim *
		frac->oldim + frac->cre;
	frac->newim = 2 * frac->oldre * frac->oldim + frac->cim;
}

void	draw_dust(t_frac *frac)
{
	frac->var.y = 0;
	while (frac->var.y < WIN_HEIGHT)
	{
		frac->var.x = 0;
		while (frac->var.x < WIN_WIDTH)
		{
			frac->newre = ((((frac->var.x - WIN_WIDTH / 2.0) * 4.0 / WIN_WIDTH)
				/ frac->zoom) + (frac->var.x / WIN_WIDTH)) + frac->movex;
			frac->newim = ((((frac->var.y - WIN_HEIGHT / 2.0) *
				(-4.0) / WIN_WIDTH) / frac->zoom)
				+ (frac->var.y / WIN_HEIGHT)) - frac->movey;
			frac->var.i = 0;
			while (frac->var.i < frac->maxiter)
			{
				dust_inside(frac);
				if ((frac->newre * frac->newre + frac->newim *
					frac->newim) > 4)
					break ;
				frac->var.i++;
			}
			place_colour(frac);
			frac->var.x++;
		}
		frac->var.y++;
	}
}
