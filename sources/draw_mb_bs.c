/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_mb_bs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:16:50 by apietush          #+#    #+#             */
/*   Updated: 2017/10/11 19:16:51 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	set_while_mb_bs(t_frac *frac)
{
	frac->var.i = 0;
	frac->pre = ((((frac->var.x - WIN_WIDTH / 2.0) * 4.0 / WIN_WIDTH)
				/ frac->zoom) + (frac->var.x / WIN_WIDTH)) + frac->movex;
	frac->pim = ((((frac->var.y - WIN_HEIGHT / 2.0) * (-4.0) / WIN_WIDTH)
				/ frac->zoom) + (frac->var.y / WIN_HEIGHT)) - frac->movey;
	frac->newre = 0;
	frac->newim = 0;
	frac->oldre = 0;
	frac->oldim = 0;
}

void	set_start_mb_bs(t_frac *frac)
{
	frac->zoom = 1;
	frac->movex = -0.5;
	frac->movey = 0;
	frac->maxiter = 50;
	frac->col_change = 0;
}

void	draw_mb(t_frac *frac)
{
	frac->var.y = 0;
	while (frac->var.y < WIN_HEIGHT)
	{
		frac->var.x = 0;
		while (frac->var.x < WIN_WIDTH)
		{
			set_while_mb_bs(frac);
			while (frac->var.i < frac->maxiter)
			{
				frac->oldre = frac->newre;
				frac->oldim = frac->newim;
				frac->newre = frac->oldre * frac->oldre - frac->oldim *
					frac->oldim + frac->pre;
				frac->newim = 2 * frac->oldre * frac->oldim + frac->pim;
				if ((frac->newre * frac->newre +
					frac->newim * frac->newim) > 4)
					break ;
				frac->var.i++;
			}
			place_colour(frac);
			frac->var.x++;
		}
		frac->var.y++;
	}
}

void	draw_ship(t_frac *frac)
{
	frac->var.y = 0;
	while (frac->var.y < WIN_HEIGHT)
	{
		frac->var.x = 0;
		while (frac->var.x < WIN_WIDTH)
		{
			set_while_mb_bs(frac);
			frac->var.i = 0;
			while (frac->var.i < frac->maxiter)
			{
				frac->oldre = frac->newre;
				frac->oldim = frac->newim;
				frac->newre = (fabs)(frac->oldre * frac->oldre -
					frac->oldim * frac->oldim + frac->pre);
				frac->newim = (fabs)(2 * frac->oldre * frac->oldim - frac->pim);
				if ((frac->newre * frac->newre +
					frac->newim * frac->newim) > 4)
					break ;
				frac->var.i++;
			}
			place_colour(frac);
			frac->var.x++;
		}
		frac->var.y++;
	}
}
