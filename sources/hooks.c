/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:17:05 by apietush          #+#    #+#             */
/*   Updated: 2017/10/11 19:17:16 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int		exit_x(void *par)
{
	par = NULL;
	exit(1);
	return (0);
}

int		zoom_event(int keycode, int x, int y, t_frac *frac)
{
	if ((frac->stop == 1 && frac->type == 1) || (frac->stop == 0 &&
		(frac->type == 2 || frac->type == 3 || frac->type == 4
		|| frac->type == 5)))
	{
		if (keycode == 4)
		{
			frac->zoom *= 1.1;
			frac->movex += (x - (WIN_WIDTH / 2)) / ((WIN_WIDTH / 2)
												/ frac->zoom) / 4;
			frac->movey += (y - (WIN_HEIGHT / 2)) / ((WIN_HEIGHT / 2)
												/ frac->zoom) / 4;
		}
		else if (keycode == 5)
		{
			frac->zoom *= 0.9;
			frac->movex -= (x - (WIN_WIDTH / 2)) / ((WIN_WIDTH / 2)
												/ frac->zoom) / 4;
			frac->movey -= (y - (WIN_HEIGHT / 2)) / ((WIN_HEIGHT / 2)
												/ frac->zoom) / 4;
		}
		render_fract(frac);
	}
	return (1);
}

int		motion(int x, int y, t_frac *frac)
{
	if (frac->type == 1 && x >= 0 && x <= WIN_WIDTH && y
		>= 0 && y <= WIN_HEIGHT && frac->stop == 0)
	{
		frac->cre = (2.0 * (x - (WIN_WIDTH / 2)) /
			(frac->zoom * (WIN_WIDTH / 2)) + frac->movex);
		frac->cim = (2.0 * (y - (WIN_HEIGHT / 2)) /
			(frac->zoom * (WIN_HEIGHT / 2)) + frac->movey);
		render_fract(frac);
	}
	return (0);
}

void	key_hook_more(int keycode, t_frac *frac)
{
	if (keycode == 78)
		frac->zoom *= 0.5;
	if (keycode == 69)
		frac->zoom *= 2;
	if (keycode == 116)
		frac->col_change += 30;
	if (keycode == 121)
		frac->col_change -= 30;
	if (keycode == 49)
	{
		if (frac->type == 1)
			set_start_julia(frac);
		else
			set_start_mb_bs(frac);
	}
	if (keycode == 36 && frac->type == 1)
	{
		if (frac->stop == 0)
			frac->stop = 1;
		else
			frac->stop = 0;
	}
}

int		key_hook(int keycode, t_frac *frac)
{
	if (keycode == 53)
	{
		mlx_destroy_window(frac->mlx.mlx_ptr, frac->mlx.win_ptr);
		exit(1);
	}
	if (keycode == 123)
		frac->movex -= (0.5 / frac->zoom);
	if (keycode == 124)
		frac->movex += (0.5 / frac->zoom);
	if (keycode == 125)
		frac->movey += (0.5 / frac->zoom);
	if (keycode == 126)
		frac->movey -= (0.5 / frac->zoom);
	if (keycode == 67)
		frac->maxiter += 10;
	if (keycode == 75)
		frac->maxiter -= 10;
	key_hook_more(keycode, frac);
	render_fract(frac);
	return (0);
}
