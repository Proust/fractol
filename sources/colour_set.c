/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colour_set.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:16:14 by apietush          #+#    #+#             */
/*   Updated: 2017/10/11 19:16:17 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	put_text(t_frac *frac)
{
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 20, 16738740, "Move camera: arrows");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 40, 16738740, "Keybord zoom: Num pad + || -");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 60, 16738740, "Change iterations: Num pad * || /");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 80, 16738740, "Play with colours: PageUp || PageDown");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 100, 16738740, "Stop || start changing Julia parameters: Enter");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 120, 16738740, "Back to the start position: Space");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 140, 16738740, "Mouse scrolling: zoom in/out");
	mlx_string_put(frac->mlx.mlx_ptr, frac->mlx.win_ptr,
		20, 160, 16738740, "Close the window: Esc || x-button");
}

char	place_rgb(int i, int index)
{
	char c_number;

	c_number = '0';
	if (i == 1)
		c_number = (unsigned char)(sin(0.016 * index + 4) * 230 + 25);
	if (i == 2)
		c_number = (unsigned char)(sin(0.013 * index + 2) * 230 + 25);
	if (i == 3)
		c_number = (unsigned char)(sin(0.01 * index + 1) * 230 + 25);
	return (c_number);
}

void	place_colour(t_frac *frac)
{
	int		index;

	if (((frac->newre * frac->newre + frac->newim * frac->newim) > 4))
	{
		index = frac->var.i - (frac->newre * frac->newre +
			frac->newim * frac->newim) + frac->col_change;
		frac->mlx.arr[(frac->var.x + (frac->var.y * WIN_WIDTH))
			* 4] = place_rgb(1, index);
		frac->mlx.arr[((frac->var.x + (frac->var.y * WIN_WIDTH)) * 4)
			+ 1] = place_rgb(2, index);
		frac->mlx.arr[((frac->var.x + (frac->var.y * WIN_WIDTH)) * 4)
			+ 2] = place_rgb(3, index);
	}
	else
	{
		frac->mlx.arr[(frac->var.x + (frac->var.y * WIN_WIDTH)) * 4] = 0;
		frac->mlx.arr[((frac->var.x + (frac->var.y * WIN_WIDTH)) * 4) + 1] = 0;
		frac->mlx.arr[((frac->var.x + (frac->var.y * WIN_WIDTH)) * 4) + 2] = 0;
	}
}
