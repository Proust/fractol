/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apietush <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 21:34:03 by apietush          #+#    #+#             */
/*   Updated: 2017/10/07 21:34:08 by apietush         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# define WIN_HEIGHT 1000
# define WIN_WIDTH 1500
# define MAXIT 20
# include "../mlx/mlx.h"
# include "../libft/libft.h"
# include <fcntl.h>
# include <math.h>
# include <stdio.h>

typedef struct		s_mlx
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img;
	char			*arr;
}					t_mlx;

typedef struct		s_var
{
	int				x;
	int				y;
	int				i;
}					t_var;

typedef struct		s_frac
{
	int				x;
	int				y;
	double			cre;
	double			cim;
	double			newre;
	double			newim;
	double			oldre;
	double			oldim;
	double			zoom;
	double			movex;
	double			movey;
	int				colour;
	int				maxiter;
	int				col_change;
	double			pre;
	double			pim;
	int				type;
	int				stop;
	t_mlx			mlx;
	t_var			var;
}					t_frac;

void				put_text(t_frac *frac);
int					zoom_event(int keycode, int x, int y, t_frac *frac);
void				render_fract(t_frac *frac);
int					motion(int x, int y, t_frac *frac);
int					exit_x(void *par);
void				set_start_mb_bs(t_frac *frac);
void				set_start_julia(t_frac *frac);
void				set_start_dust(t_frac *frac);
void				set_start_star(t_frac *frac);
int					key_hook(int keycode, t_frac *frac);
void				place_colour(t_frac *frac);
char				place_rgb(int i, int index);
int					key_hook(int keycode, t_frac *frac);
void				draw_julia(t_frac *frac);
void				draw_mb(t_frac *frac);
void				draw_ship(t_frac *frac);
void				draw_star(t_frac *frac);
void				draw_dust(t_frac *frac);

#endif
