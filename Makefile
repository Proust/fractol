#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apietush <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/07 21:31:27 by apietush          #+#    #+#              #
#    Updated: 2017/10/07 21:31:32 by apietush         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fractol
SRCS = sources
FLAGS = -Wall -Wextra -Werror -o
SRC = $(SRCS)/main.c \
		$(SRCS)/draw_julia.c \
		$(SRCS)/draw_mb_bs.c \
		$(SRCS)/colour_set.c \
		$(SRCS)/hooks.c \
		$(SRCS)/star.c \
		$(SRCS)/cosmic_dust.c \
	 
OBJ = $(SRC:.c=.o)
LIBFT = ./libft
LIB_MLX = ./mlx/libmlx.a

all: $(NAME)

$(NAME): $(OBJ)
		make -C $(LIBFT)
		make -C ./mlx
		gcc $(FLAGS) $(NAME) $(OBJ) $(LIBFT)/libft.a $(LIB_MLX) -framework OpenGL -framework AppKit

%.o: %.c
		gcc -c $(FLAGS) $@ $<

clean:
		rm -f $(OBJ)
		make clean -C $(LIBFT)
		make clean -C ./mlx

fclean: clean
		rm -f $(NAME)
		rm -f $(OBJ)
		make fclean -C $(LIBFT)

re:		fclean all
